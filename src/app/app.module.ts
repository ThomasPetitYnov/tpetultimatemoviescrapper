import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { File } from "@ionic-native/file";
import { FileChooser} from "@ionic-native/file-chooser";
import { FilePath } from "@ionic-native/file-path";
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';

import { MoviesPage } from '../pages/movies/movies';
import { SeriesPage } from "../pages/series/series";
import { FavsPage } from "../pages/favs/favs";
import { CinemaPage } from "../pages/cinema/cinema";
import { ProfilePage } from "../pages/profile/profile";
import { DetailsPage } from "../pages/details/details";
import { EpisodesPage } from "../pages/episodes/episodes";
import { EpisodesDetailsPage } from "../pages/episodes-details/episodes-details";
import { TabsPage } from "../pages/tabs/tabs";

import { OmdbApiProvider } from '../providers/omdb-api/omdb-api';
import { FavoritesHandlerProvider } from '../providers/favorites-handler/favorites-handler';
import { FileHandlerProvider } from '../providers/file-handler/file-handler';
import { FavoritesFilesHandlerProvider } from '../providers/favorites-files-handler/favorites-files-handler';

@NgModule({
  declarations: [
    MyApp,
    MoviesPage,
    SeriesPage,
    FavsPage,
    CinemaPage,
    ProfilePage,
    DetailsPage,
    EpisodesPage,
    EpisodesDetailsPage,
    TabsPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MoviesPage,
    SeriesPage,
    FavsPage,
    CinemaPage,
    ProfilePage,
    DetailsPage,
    EpisodesPage,
    EpisodesDetailsPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    OmdbApiProvider,
    HttpClientModule,
    FavoritesHandlerProvider,
    FileHandlerProvider,
    FavoritesFilesHandlerProvider,
    File,
    FileChooser,
    FilePath,
  ]
})
export class AppModule {}
