import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { ToastController } from "ionic-angular";

/*
  Generated class for the FavoritesHandlerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FavoritesHandlerProvider {

  constructor(public http: HttpClient, private storage: Storage, private toastCtrl: ToastController) {

  }

  addToFavorites(key, value, serie = null) {
    let content = JSON.stringify({
      imdbID: value.imdbID,
      Title: value.Title,
      Poster: value.Poster,
      Released: value.Released,
      Type: value.Type,
      Serie: value.Type === 'episode' ? serie : null
    });
    this.storage.get(value.Type).then(data => {
      if (data === null) {
        this.storage.set(value.Type, 1);
      } else {
        this.storage.set(value.Type, data + 1);
      }
    });
    this.storage.set(key, content);
  }

  removeFromFavorites(key, type) {
    this.storage.get(type).then(data => {
      this.storage.set(type, data - 1);
    });
    this.storage.remove(key);
  }

  isInFavorites(key) {
    return this.storage.get(key);
  }

  getFavorites() {
    return new Promise(resolve => {
      let results = [];
      this.storage.keys().then(keys => {
        keys.forEach(key => {
          this.storage.get(key).then(data => {
            if (key !== 'movie' && key !== 'series' && key !== 'episode') {
              results.push(JSON.parse(data));
            }
          });
        });
      });
      return resolve(results);
    });
  }

  createFromList(data) {
    this.storage.clear();
    let list = JSON.parse(data);
    for (let key in list) {
      this.storage.set(key, list[key]);
    }
    this.presentToast('List imported. Reload the app or switch page to see the new list !');
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom',
      showCloseButton: true
    });
    toast.present();
  }
}
