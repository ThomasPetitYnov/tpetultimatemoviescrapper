import { Component } from '@angular/core';

import {MoviesPage} from "../movies/movies";
import {SeriesPage} from "../series/series";
import {FavsPage} from "../favs/favs";
import {CinemaPage} from "../cinema/cinema";
import {ProfilePage} from "../profile/profile";

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = MoviesPage;
  tab2Root = SeriesPage;
  tab3Root = FavsPage;
  tab4Root = CinemaPage;
  tab5Root = ProfilePage;

  constructor() {}

}
