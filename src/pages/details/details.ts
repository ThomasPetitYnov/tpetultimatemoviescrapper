import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OmdbApiProvider } from "../../providers/omdb-api/omdb-api";
import { FavoritesHandlerProvider } from "../../providers/favorites-handler/favorites-handler";
import { FileHandlerProvider } from "../../providers/file-handler/file-handler";
import { EpisodesPage } from "../episodes/episodes";

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {

  id = null;
  details = [];
  isInFavorites = false;
  seasons = [];

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public omdbApi: OmdbApiProvider,
      public favoritesHandler: FavoritesHandlerProvider,
      public fileHandler: FileHandlerProvider,
  ) {
    let id = this.navParams.get('id');
    this.id = id;
    this.omdbApi.getAllDetails(this.id);
  }

  ionViewWillEnter() {
    this.details = this.omdbApi.details;
    this.favoritesHandler.isInFavorites(this.id).then(data => {
      if (data !== null) this.isInFavorites = true;
    });
  }

  addToFavorites(id, details) {
    this.favoritesHandler.addToFavorites(id, details);
    this.isInFavorites = true;
  }

  removeFromFavorites(id, type) {
    this.favoritesHandler.removeFromFavorites(id, type);
    this.isInFavorites = false;
  }

  getArrayForSeasons(totalSeasons) {
    this.seasons = [];
    for (let i = 1; i <= totalSeasons; i++) {
      this.seasons.push(i);
    }
    return this.seasons;
  }

  getEpisodes(season, title, id) {
    this.navCtrl.push(EpisodesPage, {'season': season, 'title': title, 'id': id});
  }

  downloadPoster(url) {
    this.fileHandler.download(url);
  }
}
