import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { OmdbApiProvider } from "../../providers/omdb-api/omdb-api";
import { DetailsPage } from "../details/details";
import { EpisodesDetailsPage } from "../episodes-details/episodes-details";
import { FavoritesHandlerProvider } from "../../providers/favorites-handler/favorites-handler";
import { FavoritesFilesHandlerProvider } from "../../providers/favorites-files-handler/favorites-files-handler";
import { Storage } from "@ionic/storage";

/**
 * Generated class for the FavsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
    selector: 'page-favs',
    templateUrl: 'favs.html',
})
export class FavsPage {

    favorites:any;
    nbMovies = 0;
    nbSeries = 0;
    nbEpisodes = 0;

    constructor(
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public navParams: NavParams,
        public omdbApi: OmdbApiProvider,
        public favoritesHandler: FavoritesHandlerProvider,
        private storage: Storage,
        public favoritesFileHandler: FavoritesFilesHandlerProvider
    ) {

    }

    ionViewWillEnter() {
        this.favorites = [];
        this.favoritesHandler.getFavorites().then(data => {
            this.favorites = data;
        });
        this.storage.get('movie').then(data => {
            this.nbMovies = data;
        });
        this.storage.get('series').then(data => {
            this.nbSeries = data;
        });
        this.storage.get('episode').then(data => {
            this.nbEpisodes = data;
        });
    }

    getDetails(id) {
        this.navCtrl.push(DetailsPage, {id: id});
    }

    getDetailsEpisode(id, title) {
        this.navCtrl.push(EpisodesDetailsPage, {id: id, 'serie': title});
    }

    exportFavorites() {
        let alert = this.alertCtrl.create({
            title: 'Are you sure ?',
            message: 'You will export this favorite list in a JSON file in your root directory.',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Continue',
                    handler: () => {
                        this.favoritesFileHandler.exportFavorites();
                    }
                }
            ]
        });
        alert.present();
    }

    importFavorites() {
        let alert = this.alertCtrl.create({
            title: 'Warning !',
            message: 'By importing a file, you\'ll overwrite your favorites. Be sure before selecting a (JSON) file !',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                },
                {
                    text: 'Continue',
                    handler: () => {
                        this.favoritesFileHandler.importFavorites();
                    }
                }
            ]
        });
        alert.present();
    }
}
