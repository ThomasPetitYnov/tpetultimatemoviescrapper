import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { OmdbApiProvider } from "../../providers/omdb-api/omdb-api";
import { DetailsPage } from "../details/details";

@Component({
    selector: 'page-series',
    templateUrl: 'series.html',
})
export class SeriesPage {

    private isOpenned: boolean = false;
    public seriesData = [];

    constructor(public navCtrl: NavController, public omdbApi: OmdbApiProvider) {

    }

    getState() {
        return this.isOpenned;
    }

    onInput(el) {
        let needle = el.target.value.trim();
        if (needle.length > 3) {
            let params = "&page=1&type=series&s=" + needle.split(' ').join('+') + "*";
            this.omdbApi.getSeries(params);
            this.seriesData = this.omdbApi.series;
        }
    }

    openSearchBar() {
        this.isOpenned = true;
    }

    getDetails(id) {
        this.navCtrl.push(DetailsPage, {id: id});
    }

}
