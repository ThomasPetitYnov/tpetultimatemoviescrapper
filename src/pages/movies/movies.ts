import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { OmdbApiProvider } from "../../providers/omdb-api/omdb-api";
import { DetailsPage } from "../details/details";

@Component({
    selector: 'page-movies',
    templateUrl: 'movies.html'
})
export class MoviesPage {

    private isOpenned: boolean = false;
    public moviesData = [];

    constructor(public navCtrl: NavController, public omdbApi: OmdbApiProvider) {

    }

    getState() {
        return this.isOpenned;
    }

    onInput(el) {
        let needle = el.target.value.trim();
        if (needle.length > 3) {
            let params = "&page=1&type=movie&s=" + needle.split(' ').join('+') + "*";
            this.omdbApi.getMovies(params);
            this.moviesData = this.omdbApi.movies;
        }
    }

    openSearchBar() {
        this.isOpenned = true;
    }

    getDetails(id) {
        this.navCtrl.push(DetailsPage, {id: id});
    }
}
