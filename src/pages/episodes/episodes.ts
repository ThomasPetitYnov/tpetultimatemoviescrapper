import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OmdbApiProvider } from "../../providers/omdb-api/omdb-api";
import { EpisodesDetailsPage } from "../episodes-details/episodes-details";

/**
 * Generated class for the EpisodesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-episodes',
  templateUrl: 'episodes.html',
})
export class EpisodesPage {

  season = null;
  title = null;
  id = null;
  episodes = [];

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public omdbApi: OmdbApiProvider)
  {
    let season = this.navParams.get('season');
    this.season = season;
    let title = this.navParams.get('title');
    this.title = title;
    let id = this.navParams.get('id');
    this.id = id;
    this.omdbApi.getEpisodesForASeason(this.id, this.season);
  }

  ionViewDidLoad() {
    this.episodes = this.omdbApi.episodes;
  }

  goToDetails(id, serie) {
    this.navCtrl.push(EpisodesDetailsPage, {'id': id, 'serie': serie});
  }

}
