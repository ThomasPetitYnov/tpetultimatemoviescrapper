import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { OmdbApiProvider } from "../../providers/omdb-api/omdb-api";
import { FavoritesHandlerProvider } from "../../providers/favorites-handler/favorites-handler";
import { FileHandlerProvider } from "../../providers/file-handler/file-handler";

/**
 * Generated class for the EpisodesDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-episodes-details',
  templateUrl: 'episodes-details.html',
})
export class EpisodesDetailsPage {

  id = null;
  serie = null;
  details = [];
  isInFavorites = false;

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public omdbApi: OmdbApiProvider,
      public favoritesHandler: FavoritesHandlerProvider,
      public fileHandler: FileHandlerProvider,
  ) {
    let id = this.navParams.get('id');
    this.id = id;
    let serie = this.navParams.get('serie');
    this.serie = serie;
    this.omdbApi.getAllDetails(this.id);
  }

  ionViewDidLoad() {
    this.details = this.omdbApi.details;
    this.favoritesHandler.isInFavorites(this.id).then(data => {
      if (data !== null) this.isInFavorites = true;
    });
  }

  addToFavorites(id, details, serie) {
    this.favoritesHandler.addToFavorites(id, details, serie);
    this.isInFavorites = true;
  }

  removeFromFavorites(id, type) {
    this.favoritesHandler.removeFromFavorites(id, type);
    this.isInFavorites = false;
  }

  downloadPoster(url) {
    this.fileHandler.download(url);
  }
}
