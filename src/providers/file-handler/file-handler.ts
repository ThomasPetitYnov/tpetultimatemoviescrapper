import { Injectable } from '@angular/core';
import { File } from '@ionic-native/file';
import { ToastController } from "ionic-angular";
import { FilePath } from "@ionic-native/file-path";
import { FavoritesHandlerProvider } from "../favorites-handler/favorites-handler";

/*
  Generated class for the FileHandlerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FileHandlerProvider {

  constructor(
      private file: File,
      private toastCtrl: ToastController,
      private filePath: FilePath,
      private favoritesHandler: FavoritesHandlerProvider,
  ) {

  }

  download(url) {
    let file = this.file;
    let toastCtrl = this.toastCtrl;
    let oReq = new XMLHttpRequest();

    oReq.open("GET", url, true);
    oReq.responseType = "blob";

    oReq.onload = function (oEvent) {
      file.writeFile(file.externalDataDirectory, 'tmp.jpg', oReq.response, { replace: true }).then( data => {
        (<any>window).cordova.plugins.imagesaver.saveImageToGallery(file.externalDataDirectory+'tmp.jpg', onSaveImageSuccess, onSaveImageError);

        function onSaveImageSuccess(){
          presentToast('File downloaded successfully !');
        }
        function onSaveImageError(error) {
          presentToast('Download failed...');
        }

        function presentToast(message) {
          let toast = toastCtrl.create({
            message: message,
            duration: 5000,
            position: 'bottom',
            showCloseButton: true
          });
          toast.present();
        }
      });
    };
    oReq.send();
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      position: 'bottom',
      showCloseButton: true
    });
    toast.present();
  }

  writeJSON(filename, object) {
    return this.file.writeFile(this.file.externalRootDirectory, filename, object, {replace:true}).then(_ => {
      this.presentToast('JSON file downloaded in your root directory !');
      console.log('Directory exists: ' + this.file.externalRootDirectory);
    }).catch(_ => {
      console.log('Directory doesn\'t exist');
    });
  }

  getJSON(uri) {
    this.filePath.resolveNativePath(uri).then(path => {
      let splitedPath = path.split('/');
      let fileName = splitedPath.pop();
      let fullPath = splitedPath.join('/');
      this.file.readAsText(fullPath, fileName).then(data => {
        this.favoritesHandler.createFromList(data);
      });
    });
  }

}
