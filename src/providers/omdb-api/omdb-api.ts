import { HttpClient } from '@angular/common/http';
import { Injectable} from '@angular/core';
import "rxjs/add/operator/map"

/*
  Generated class for the OmdbApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OmdbApiProvider {

    baseUri = 'http://www.omdbapi.com/?apikey=75522b56';
    movies = [];
    series = [];
    errorMovies = "";
    errorSeries = "";
    details = [];
    favorites = [];
    episodes = [];

    constructor(public http: HttpClient) {

    }

    getData(url) {
        return this.http.get(url).map(res => res);
    }

    getMovies(params) {
        this.movies = [];
        this.errorMovies = "";
        this.getData(this.baseUri + params).subscribe(data => {
            if (data['Error'] === "To many results.") {
                this.errorMovies = "To many results.";
                return;
            } else if (data['Error'] === "Movie not found!") {
                this.errorMovies = "Movies not found!";
                return
            }
            for (let d of data['Search']) {
                this.getSomeDetails(d['imdbID'], "movie");
            }
        });
    }

    getSeries(params) {
        this.series = [];
        this.errorSeries = "";
        this.getData(this.baseUri + params).subscribe(data => {
            if (data['Error'] === "To many results.") {
                this.errorSeries = "To many results.";
                return;
            } else if (data['Error'] === "Series not found!") {
                this.errorSeries = "Series not found!";
                return
            }
            for (let d of data['Search']) {
                this.getSomeDetails(d['imdbID'], "series");
            }
        });
    }

    getSomeDetails(id, type) {
        let movieId = '&i=' + id;
        this.getData(this.baseUri + "&plot=short&" + type + movieId).subscribe(data => {
            let array = [];
            for (let key in data) {
                array[key] = data[key];
            }
            if (type === 'movie') {
                this.movies.push(array);
            } else {
                this.series.push(array);
            }
        });
    }

    getAllDetails(id) {
        this.details = [];
        let movieId = '&i=' + id;
        this.getData(this.baseUri + "&plot=full" + movieId).subscribe(data => {
            for (let key in data) {
                this.details[key] = data[key];
            }
        });
    }

    getFavorites(ids) {
        this.favorites = [];
        for (let id of ids) {
            this.getOneFavorite(id);
        }
        return this.favorites;
    }

    getOneFavorite(id) {
        let movieId = '&i=' + id;
        this.getData(this.baseUri + "&plot=short" + movieId).subscribe(data => {
            let array = [];
            for (let key in data) {
                array[key] = data[key];
            }
            return array;
        });
    }

    getEpisodesForASeason(id, season) {
        this.episodes = [];
        let serieId = '&i=' + id;
        let seasonNumber = '&Season=' + season;
        this.getData(this.baseUri + serieId + seasonNumber).subscribe(data => {
            for (let episode in data['Episodes']) {
                this.episodes.push(data['Episodes'][episode]);
            }
        });
    }
}
