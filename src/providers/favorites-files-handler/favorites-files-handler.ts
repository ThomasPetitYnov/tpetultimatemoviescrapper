import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from "@ionic/storage";
import { FavoritesHandlerProvider } from "../favorites-handler/favorites-handler";
import { FileHandlerProvider } from "../file-handler/file-handler";
import { FileChooser } from "@ionic-native/file-chooser";

/*
  Generated class for the FavoritesFilesHandlerProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class FavoritesFilesHandlerProvider {

  constructor(
      public http: HttpClient,
      public storage: Storage,
      public favoritesHandler: FavoritesHandlerProvider,
      public fileHandler: FileHandlerProvider,
      private fileChooser: FileChooser
  ) {

  }

  exportFavorites() {
    let content: any = {};
    this.storage.forEach((value, key) => {
      content[key] = value;
    });
    this.fileHandler.writeJSON('favs.json', content);
  }

  importFavorites() {
    this.fileChooser.open().then(uri => {
      this.fileHandler.getJSON(uri);
    }).catch(_ => {
      console.log('error');
    });

  }

}
